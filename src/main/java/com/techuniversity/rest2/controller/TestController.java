package com.techuniversity.rest2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/test")
public class TestController {
    @GetMapping("/saludos")
    public String saludar( @RequestParam(value="nombre", required = false,
                    defaultValue = "Lorena") String name){
        return String.format("Saludos %s", name);
    }

    @GetMapping("/varios")
    public String varios( @RequestParam Map<String, String> allParams){
        return "Los parámetros son: " + allParams.entrySet();
    }

    @GetMapping("/multi")
    public String multi( @RequestParam List<String> ids){
        return "Los parámetros son: " + ids;
    }
}
